import json
import http.client

from flask import Flask
from flask import render_template


def _get_houses() -> list:
    conn = http.client.HTTPSConnection(_URL)
    conn.request('GET', '/api/houses')
    resp = conn.getresponse()
    body = resp.read().decode('utf-8')
    body_json = json.loads(body)
    houses_details = [(house.get('name', ''), house.get('swornMembers', '')) for house in body_json]

    return houses_details


def _get_sworn_member(house: str) -> list:
    end_point = [record[1] for record in _HOUSES if house == record[0]][0]

    return end_point


def _get_members_name(member: str) -> str:
    member_url = member.replace('https://www.anapioficeandfire.com', '')
    conn = http.client.HTTPSConnection(_URL)
    conn.request('GET', member_url)
    resp = conn.getresponse()
    body = resp.read().decode('utf-8')
    body_json = json.loads(body)
    members_name = body_json.get('name', '')

    return members_name


_URL = 'www.anapioficeandfire.com'
_HOUSES = _get_houses()

app = Flask(__name__)


@app.route('/')
def _houses():

    return render_template('houses.html', houses=_HOUSES)


@app.route('/<house>')
def _sworn_member(house: str):
    sworn_members = _get_sworn_member(house)
    sworn_members_names = []
    for member in sworn_members:
        members_name = _get_members_name(member)
        sworn_members_names.append(members_name)

    return render_template('houses.html', houses=_HOUSES, sworn_members_names=sworn_members_names)


if __name__ == '__main__':
    app.run()
